@extends('master.layout')
@section('title' ,'login')
@section('content')
    <form method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="email">user or email:</label>
            <input type="text" class="form-control" id="email" name="email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" name="password" id="pwd">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
@endsection