<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_name',20)->comment('user name');
            $table->string('user_email',20)->comment('user email');
            $table->string('user_pass',64)->comment('pass word');
            $table->tinyInteger('user_type')->default(0)->comment('phan loai user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
