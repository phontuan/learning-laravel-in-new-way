<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    protected $primaryKey = 'user_id';
    protected $fillable = [
      'user_name', 'user_email', 'user_pass', 'user_type'
    ];

    public function getAuthPassword()
    {
        return $this->user_pass;
    }
}
