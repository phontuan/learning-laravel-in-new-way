<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('login');
    }

    public function postLogin(Request $request)
    {
        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'user_email' : 'user_name';

        if (Auth::attempt([$field => $request->email, 'password' => $request->password])) {
            return 'ngon';
        } else {
            return 'xit';

        }

    }
}
